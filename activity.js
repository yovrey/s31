
 // 1. What directive is used by Node.js in loading the modules it needs?
// Ans: the require direvative


// 2. What Node.js module contains a method for server creation?
// Ans: the http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
// Ans: the .createServer method of the http object


// 4. What method of the response object allows us to set status codes and content types?
// Ans: the request.writeHead method  of the response object
	


// 5. Where will console.log() output its contents when run in Node.js?
// Ans: console.log() output its contents when run in Node.js in the terminal


// 6. What property of the request object contains the address's endpoint?
// Ans: the request.url property of the request object